r-cran-maldiquant (1.22.3-2) UNRELEASED; urgency=medium

  * Replace Filippo + Sebastian by myself as Uploader (Thank you Filippo
    and Sebastian for your previous work on this package)

 -- Andreas Tille <tille@debian.org>  Thu, 09 Jan 2025 07:35:52 +0100

r-cran-maldiquant (1.22.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Tue, 10 Sep 2024 13:40:30 +0900

r-cran-maldiquant (1.22.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 26 Jan 2024 23:01:14 +0100

r-cran-maldiquant (1.22.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Andreas Tille <tille@debian.org>  Sat, 24 Jun 2023 23:16:14 +0200

r-cran-maldiquant (1.22-1) unstable; urgency=medium

  * Team upload.
  * Disable reprotest
  * Standards-Version: 4.6.1 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 15 Nov 2022 11:54:45 +0100

r-cran-maldiquant (1.21-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 01 Jan 2022 08:53:45 +0100

r-cran-maldiquant (1.20-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 15 Sep 2021 17:29:06 +0200

r-cran-maldiquant (1.19.3-2) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Fri, 04 Sep 2020 09:05:40 +0200

r-cran-maldiquant (1.19.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0

 -- Andreas Tille <tille@debian.org>  Mon, 15 Jul 2019 20:57:24 +0200

r-cran-maldiquant (1.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 09 Jul 2018 09:02:22 +0200

r-cran-maldiquant (1.17-2) unstable; urgency=medium

  * Team upload.
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends
  * Drop unneeded get-orig-source target

 -- Andreas Tille <tille@debian.org>  Thu, 21 Jun 2018 05:32:51 +0200

r-cran-maldiquant (1.17-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Standards-Version: 4.1.3
  * debhelper 11
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Sun, 11 Mar 2018 18:08:48 +0100

r-cran-maldiquant (1.16.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Standards-Version: 4.1.1
  * Add README.source to document binary data files

 -- Andreas Tille <tille@debian.org>  Sun, 01 Oct 2017 00:19:03 +0200

r-cran-maldiquant (1.16.1-1) unstable; urgency=low

  * New upstream release.

 -- Sebastian Gibb <sgibb.debian@gmail.com>  Sun, 15 Jan 2017 17:00:16 +0100

r-cran-maldiquant (1.16-1) unstable; urgency=low

  * New upstream release.

 -- Sebastian Gibb <sgibb.debian@gmail.com>  Wed, 14 Dec 2016 13:22:47 +0100

r-cran-maldiquant (1.15-1) unstable; urgency=low

  * Team upload

  [ Sebastian Gibb ]
  * New upstream release.
  * debian/control:
      - Set Standards-Version to 3.9.8.
      - Use HTTPS in Homepage, Vcs-Git and Vcs-Browser field.
      - Use canonical URL in Homepage.
      - Fix URL in Vcs-Git.

  [ Andreas Tille ]
  * cme fix dpkg-control
  * Convert to dh-r
  * debhelper 10
  * Update d/copyright
  * d/watch: version=4
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Mon, 12 Dec 2016 14:14:31 +0100

r-cran-maldiquant (1.12-1) unstable; urgency=low

  * New upstream release.
  * debian/control: set Standards-Version to 3.9.6.
  * debian/copyright:
      - Change GPL-3 to GPL-3+.
      - Remove duplicated license text.
  * upload by Filippo Rusconi <lopippo@debian.org>.

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 15 Jun 2015 13:11:57 +0200

r-cran-maldiquant (1.11-1) unstable; urgency=low

  * New upstream release.
  * Move debian/upstream to debian/upstream/metadata.
  * Upload by Filippo Rusconi <lopippo@debian.org>

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 18 Aug 2014 13:04:34 +0200

r-cran-maldiquant (1.10-2) unstable; urgency=low

  * debian/rules: remove custom build flags. This change will prevent the
    build errors encountered when using make 4.0
    (Message-ID: <87vbtsad2h.fsf@glaurung.internal.golden-gryphon.com>).

  * Upload by Filippo Rusconi <lopippo@debian.org>

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Tue, 29 Apr 2014 13:20:21 +0100

r-cran-maldiquant (1.10-1) unstable; urgency=low

  * New upstream release.
  * Upload by Filippo Rusconi <lopippo@debian.org>

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Sat, 12 Apr 2014 10:41:42 +0100

r-cran-maldiquant (1.9-1) unstable; urgency=low

  * New upstream release.
  * Replace own build script by a cdbs based one:
    - debian/control.in: removed.
    - debian/control: add cdbs to Build-Depends.
    - debian/rules: use cdbs based build script.
  * debian/rules: set custom makeFlags to harden MALDIquant.so.
  * debian/control: set Standards-Version to 3.9.5.
  * Upload by Filippo Rusconi <lopippo@debian.org>

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Tue, 14 Jan 2014 19:54:17 +0100

r-cran-maldiquant (1.8-1) unstable; urgency=low

  * New upstream release.
  * Remove debian/lintian-overrides, hardening-no-fortify-functions is not
    needed anymore.
  * debian/control.in: use canonical URI in Vcs-* fields.
  * Upload by Filippo Rusconi <lopippo@debian.org>.

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Sun, 08 Sep 2013 18:00:16 +0200

r-cran-maldiquant (1.7-1) unstable; urgency=low

  * New upstream release.
  * debian/lintian-overrides: add hardening-no-fortify-functions.
  * debian/{compat,control.in}: set debhelper (>=9).
  * debian/rules:
    - Remove useless LICENSE removing statement.
    - Add CFLAGS and CPPFLAGS.
    - Remove own LDFLAGS definition (is set automatically by debhelper >= 9).

  * Uploaded by Filippo Rusconi <lopippo@debian.org>.

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Thu, 30 May 2013 07:43:31 +0200

r-cran-maldiquant (1.6-1) unstable; urgency=low

  * New upstream release.

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Sat, 02 Mar 2013 11:36:28 +0100

r-cran-maldiquant (1.5-1) unstable; urgency=low

  * New upstream release.

  * Uploaded by Filippo Rusconi <lopippo@debian.org>.

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 10 Dec 2012 17:01:20 +0100

r-cran-maldiquant (1.4-1) unstable; urgency=low

  * Initial release by Sebastian Gibb <sgibb.debian@gmail.com>
    (Closes: #693919).

  * Uploaded by Filippo Rusconi <lopippo@debian.org>.

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Sun, 18 Nov 2012 21:55:27 +0100
